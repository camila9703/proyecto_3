
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/30/2015 17:11:54
-- Generated from EDMX file: C:\Users\Usuario TI\Documents\Visual Studio 2013\Projects\tercera_Entrega\tercera_Entrega\Models\ERFinalFinal.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [BDFinalFinalST];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[ProductosSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ProductosSet];
GO
IF OBJECT_ID(N'[dbo].[PaginasSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PaginasSet];
GO
IF OBJECT_ID(N'[dbo].[MenuSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MenuSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'ProductosSet'
CREATE TABLE [dbo].[ProductosSet] (
    [Id_productos] int IDENTITY(1,1) NOT NULL,
    [nombre] nvarchar(max)  NOT NULL,
    [precio] nvarchar(max)  NOT NULL,
    [cantidad] nvarchar(max)  NOT NULL,
    [descripcion] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'PaginasSet'
CREATE TABLE [dbo].[PaginasSet] (
    [Id_paginas] int IDENTITY(1,1) NOT NULL,
    [titulo] nvarchar(max)  NOT NULL,
    [contenido] nvarchar(max)  NOT NULL,
    [nombre] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'MenuSet'
CREATE TABLE [dbo].[MenuSet] (
    [Id_menu] int IDENTITY(1,1) NOT NULL,
    [nombre] nvarchar(max)  NOT NULL,
    [contenido] nvarchar(max)  NOT NULL,
    [titulo] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id_productos] in table 'ProductosSet'
ALTER TABLE [dbo].[ProductosSet]
ADD CONSTRAINT [PK_ProductosSet]
    PRIMARY KEY CLUSTERED ([Id_productos] ASC);
GO

-- Creating primary key on [Id_paginas] in table 'PaginasSet'
ALTER TABLE [dbo].[PaginasSet]
ADD CONSTRAINT [PK_PaginasSet]
    PRIMARY KEY CLUSTERED ([Id_paginas] ASC);
GO

-- Creating primary key on [Id_menu] in table 'MenuSet'
ALTER TABLE [dbo].[MenuSet]
ADD CONSTRAINT [PK_MenuSet]
    PRIMARY KEY CLUSTERED ([Id_menu] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------