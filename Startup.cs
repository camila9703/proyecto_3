﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(tercera_Entrega.Startup))]
namespace tercera_Entrega
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
